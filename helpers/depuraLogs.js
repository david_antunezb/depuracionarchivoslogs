const fs = require("fs");
const path = require('path');
let RUTAS_LOGS = ['C://LOGS', 'C://LOGS//LOGS_ANTUNEZ', 'C://LOGS//LOGS_PRUEBA'];

/** Función para buscar los archivos contenidos en los diferentes paths proporcionados
 * por el array.
 * Si encuentra carpetas dentro de esa ubicación la ignora, filtra solo archivos.
 *  
 * @author David Antunez Barbosa
 * @date   4 de Junio 2022 
 */
const encuentraArchivos = async () => {
    RUTAS_LOGS.forEach(ruta_log => {
        fs.readdir(ruta_log, function (err, archivos) {
            if (err) {
                console.log(err);
            }else{
                archivos.forEach(archivo => {
                    if(!fs.lstatSync(`${ruta_log}//${archivo}`).isDirectory() ){
                        let ruta_completa = `${ruta_log}//${archivo}`;
                        filtroArchivoEliminar(ruta_completa);
                    }
                })
            }
        })
    });
}

/**
 * Función para filtrar los archivos que se van a eliminar.
 * Verifica si se trata de un archivo con extensión .log, caso contrario lo ignora.
 * Verifica si el archivo recibido tiene una antiguedad de 5 días o más, si es así lo envía a eliminar
 * @param {*} archivo    Archivo Eliminado
 * 
 */
const filtroArchivoEliminar = (archivo) =>{    
    fs.stat(archivo, (err, stats)=>{
        if ( path.extname(archivo) === '.log' ){
            let milisegundos_creacion = stats.mtimeMs;
            let creacion_archivo = convertMilisToDays(milisegundos_creacion);      
            
            let hoy =  Date.now();
            let fecha_actual = convertMilisToDays(hoy);

            let antiguedad_archivo = fecha_actual - creacion_archivo;
            if ( antiguedad_archivo > 5 )
                eliminarArchivo(archivo);
            else 
                console.log("Se conservará archivo: " + archivo);
        }else{
            console.log(`${archivo} NO es un log`);
        }
    });
}


const convertMilisToDays = (miliseconds) => {
    return miliseconds / 1000 / 60 / 60 / 24;
}


const eliminarArchivo = (archivo) => {
    fs.unlink(archivo, (err) => {
        if (err) throw err;
        console.log(`Archivo eliminado: ${archivo}`)
    });
}


module.exports = { encuentraArchivos }